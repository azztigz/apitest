<?php

namespace App\Http\Controllers\Front;


use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class WelcomeController extends Controller
{
    function index()
    {

    	$client = new Client(); //GuzzleHttp\Client

		$res = $client->post('http://api.loc/api/auth/login', [
		    'form_params' => [
		        'email' => 'test1@test.com',
		        'password' => '123456'
		    ]
		]);

		$res_json =  json_decode($res->getBody());

		Session::put('api_token', $res_json->token);

		echo '<pre>';
		print_r($res_json->token);
		echo '</pre>';

		$res = $client->get('http://api.loc/api/book', [
		    'headers' => [
		        'Authorization' => 'bearer '.$res_json->token,
		    ]
		]);

		$book_json = json_decode($res->getBody());

		echo '<pre>';
		print_r($book_json);
		echo '</pre>';

		/*$res = $client->post('http://api.loc/api/auth/logout', [
		    'headers' => [
		        'Authorization' => 'bearer '.Session::get('api_token'),
		    ]
		]);*/

		//echo Session::get('api_token');

    	$data['test'] = 'test';
    	//return view('front.welcome', $data);
    }
}
